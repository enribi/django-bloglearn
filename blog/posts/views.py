from django.views.generic import UpdateView, DeleteView, CreateView,ListView,DetailView
from django.urls import reverse_lazy
from .models import Posts, FeaturedPost
from .forms import CommentForm, PostForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from rest_framework import viewsets
from .serializers import PostsSerializer, FeaturedPostSerializer
from rest_framework import generics


# functional Based views
def home(request):
    posts = Posts.objects.order_by('-date_posted')
    featured_posts = FeaturedPost.objects.all().order_by('priority')
    context = {'posts': posts, 'featured_posts': featured_posts}
    return render(request, 'home.html', context)


def post_detail(request, pk):
    post = get_object_or_404(Posts, pk=pk)
    user_id = request.session.get('id')
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        comment_form = CommentForm()

    context = {
        'post': post,
        'comments': post.comments.all(),
        'comment_form': comment_form
    }
    return render(request, 'posts/post_detail.html', context)


# class based Views
class HomeView(ListView):
    model = Posts
    template_name = 'home.html'
    context_object_name = 'posts'
    ordering = ['title']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['featured_posts'] = FeaturedPost.objects.all().order_by('priority')
        visits_count = self.request.session.get('visits_count', 0)
        self.request.session['visits_count'] = visits_count + 1
        context['count'] = visits_count
        return context


class PostDetailView(LoginRequiredMixin, DetailView):
    model = Posts
    template_name = 'posts/post_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comments'] = self.object.comments.all()
        context['post'] = self.object
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.post = self.object
            comment.save()
            return redirect('post_detail', pk=self.object.id)
        context = self.get_context_data(object=self.object)
        context['comment_form'] = comment_form
        return self.render_to_response(context)


class PostCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')
    permission_required = 'posts.add_posts'


class PostUpdateView(LoginRequiredMixin,PermissionRequiredMixin, UpdateView):
    model = Posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')


class PostDeleteView(LoginRequiredMixin, DeleteView):
    model = Posts
    template_name = 'posts/post_confirm_delete.html'
    success_url = reverse_lazy('home')
    permission_required = 'posts.delete_posts'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post = self.get_object()
        context['post'] = post
        return context

#*************** REST FRAMEWORK *******************


class PostViewSet(viewsets.ModelViewSet):
    queryset = Posts.objects.all()
    serializer_class = PostsSerializer


class FeaturedPostViewSet(viewsets.ModelViewSet):
    queryset = FeaturedPost.objects.all()
    serializer_class = FeaturedPostSerializer
